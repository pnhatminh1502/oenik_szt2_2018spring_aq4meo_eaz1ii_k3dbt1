﻿// <copyright file="IDataManipulation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq.Expressions;

    /// <summary>
    /// The DataManipulation interface
    /// </summary>
    /// <typeparam name="T">T type</typeparam>
    public interface IDataManipulation<T>
    {
        /// <summary>
        /// Add a new T object to the database
        /// </summary>
        /// <param name="entity">The new object</param>
        void Add(T entity);

        /// <summary>
        /// Edit a T object
        /// </summary>
        /// <param name="entity">The T object what we want to edit</param>
        void Edit(T entity);

        /// <summary>
        /// Remove the T object from the database
        /// </summary>
        /// <param name="entity">The T object what we want to delete</param>
        void Remove(T entity);

        /// <summary>
        /// Get all of the T object from the database
        /// </summary>
        /// <returns> The list of the T items </returns>
        IList<T> GetAll();

        /// <summary>
        /// Find T elements according lambda expression
        /// </summary>
        /// <param name="predicate">The lambda expression for filtering</param>
        /// <returns>Return list of T item</returns>
        IList<T> Find(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Return the object if exists in the database using id.
        /// </summary>
        /// <param name="id">the id of the object</param>
        /// <returns>the T object</returns>
        T GetById(int id);
    }
}