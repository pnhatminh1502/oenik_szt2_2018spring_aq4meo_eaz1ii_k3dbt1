﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// The Irepository interface
    /// </summary>
    /// <typeparam name="T">T type</typeparam>
    /// <seealso cref="Data.IDataManipulation{T}" />
    public interface IRepository<T> : IDataManipulation<T>
        where T : class
    {
    }
}
