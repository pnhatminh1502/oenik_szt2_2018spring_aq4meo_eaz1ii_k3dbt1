﻿IF OBJECT_ID('Meter') IS NOT NULL DROP TABLE dbo.Meter;
IF OBJECT_ID('Meter15m') IS NOT NULL DROP TABLE dbo.Meter15m; 
-- IF OBJECT_ID('User') IS NOT NULL DROP TABLE dbo.User; 
IF OBJECT_ID('Tariff') IS NOT NULL DROP TABLE dbo.Tariff; 
IF OBJECT_ID('Bill') IS NOT NULL DROP TABLE dbo.Bill; 
IF OBJECT_ID('Contract') IS NOT NULL DROP TABLE dbo.Contract; 
IF OBJECT_ID('EnergyType') IS NOT NULL DROP TABLE dbo.EnergyType;
IF OBJECT_ID('Company') IS NOT NULL DROP TABLE dbo.Company; 

CREATE TABLE [dbo].[Company] (
    [id]      INT          IDENTITY (1, 1) NOT NULL,
    [name]    VARCHAR (30) NOT NULL,
    [address] NCHAR (100)  NOT NULL,
    [email]   NCHAR (30)   NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


CREATE TABLE [dbo].[User] (
    [id]             INT           IDENTITY (1, 1) NOT NULL,
    [username]       VARCHAR (50)  NOT NULL,
    [loginname]      VARCHAR (50)  NOT NULL,
    [password]       NVARCHAR (50) NOT NULL,
    [algorithm]      NVARCHAR (30) NOT NULL,
    [salt]           NVARCHAR (50) NOT NULL,
    [tokenname]      NVARCHAR (50) NULL,
    [permission_lvl] INT           NOT NULL,
    [company_id]     INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    FOREIGN KEY ([company_id]) REFERENCES [dbo].[Company] ([id])
);

CREATE TABLE [dbo].[EnergyType] (
    [id]   INT        IDENTITY (1, 1) NOT NULL,
    [name] NVARCHAR (15) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

CREATE TABLE [dbo].[Meter] (
    [id]				 INT           IDENTITY (1, 1) NOT NULL,
    [name]				 VARCHAR (50)  NOT NULL,
    [pod_identifier]	 VARCHAR (11)  NOT NULL,
	[energy_type_id]	 INT		   NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    FOREIGN KEY ([energy_type_id]) REFERENCES [dbo].[EnergyType] ([id])
);

CREATE TABLE [dbo].[Meter15m] (
    [id]				 INT           IDENTITY (1, 1) NOT NULL,
    [meter_id]			 INT		   NOT NULL,
	[date_from]			 DATE		   NOT NULL,
	[date_to]			 DATE		   NOT NULL,
	[value]				 FLOAT		   NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    FOREIGN KEY ([meter_id]) REFERENCES [dbo].[Meter] ([id])
);

CREATE TABLE [dbo].[Contract] (
    [id]              INT           IDENTITY (1, 1) NOT NULL,
    [name]            NVARCHAR (45) NOT NULL,
    [contract_number] INT           NOT NULL,
    [date_valid_from] DATETIME      NOT NULL,
    [date_valid_to]   DATETIME      NOT NULL,
    [company_id]	  INT           NULL,
    [energy_type_id]  INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    FOREIGN KEY ([energy_type_id]) REFERENCES [dbo].[EnergyType] ([id]),
    FOREIGN KEY ([company_id]) REFERENCES [dbo].[Company] ([id])
);

CREATE TABLE [dbo].[Bill] (
    [id]              INT           IDENTITY (1, 1) NOT NULL,
    [name]            NVARCHAR (45) NOT NULL,
    [bill_number]	  INT           NOT NULL,
    [date_from]		  DATETIME      NOT NULL,
    [date_to]		  DATETIME      NOT NULL,
	[contract_id]	  INT			NOT NULL,
	[meter_id]		  INT			NOT NULL,
	[is_valid]		  BIT			NOT NULL,
	[comment]		  TEXT			NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    FOREIGN KEY ([contract_id]) REFERENCES [dbo].[Contract] ([id]),
    FOREIGN KEY ([meter_id]) REFERENCES [dbo].[Meter] ([id])
);


CREATE TABLE [dbo].[Tariff] (
    [id]             INT          IDENTITY (1, 1) NOT NULL,
    [unit]           VARCHAR (10) NULL,
    [value]          INT          NULL,
    [date_from]      DATETIME     NULL,
    [date_to]        DATETIME     NULL,
    [energy_type_id] INT          NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    FOREIGN KEY ([energy_type_id]) REFERENCES [dbo].[EnergyType] ([id])
);

