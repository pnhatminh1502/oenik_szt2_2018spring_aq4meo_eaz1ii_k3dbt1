﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// The repository class
    /// </summary>
    /// <typeparam name="T"> T type</typeparam>
    /// <seealso cref="Data.IRepository{T}" />
    public class Repository<T> : IRepository<T>
        where T : class
    {
        private readonly Model.Database db;
        private DbSet<T> dbSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="db">The database.</param>
        public Repository(Model.Database db)
        {
            this.db = db;
            this.dbSet = db.Set<T>();
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Returns the id</returns>
        public T GetById(int id)
        {
            return this.dbSet.Find(id);
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>Observablecollection T</returns>
        public IList<T> GetAll()
        {
            return new ObservableCollection<T>(this.dbSet.ToList<T>());
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Add(T entity)
        {
            this.dbSet.Add(entity);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Finds the specified predicate.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns> The list of objects which satisfy the predicate</returns>
        public IList<T> Find(Expression<Func<T, bool>> predicate)
        {
            return this.dbSet.Where(predicate).ToList<T>();
        }

        /// <summary>
        /// Removes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Remove(T entity)
        {
            this.dbSet.Remove(entity);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Edits the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Edit(T entity)
        {
            this.db.Entry<T>(entity).State = System.Data.Entity.EntityState.Modified;
            this.db.SaveChanges();
        }
    }
}
