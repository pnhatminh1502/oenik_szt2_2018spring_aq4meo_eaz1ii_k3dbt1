﻿using Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface IUserMod
    {
        void AddUser(User User);
        void DelUser(int UserID);
        void EditUser(User User);
        IQueryable<User> GetAllUsers();
    }

    public interface ICompany
    {
        void CreateCompany(int Id, string Name);
        void RemoveCompany(int CompanyID);
    }

    public interface IBill
    {
        void AddBill(int Id, string Name, int Amount, string Unit, DateTime DateFrom, DateTime DateTo);
        void EditBill(int Id);
    }

    public interface IContract
    {
        void CreateContract(int ID, int BillID, int ContractID);
        void DeleteContract(int ID);
    }

    public class Data
    {
    }
}
