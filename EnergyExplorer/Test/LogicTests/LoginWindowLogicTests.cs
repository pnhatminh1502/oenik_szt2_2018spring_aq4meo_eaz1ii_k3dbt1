﻿// <copyright file="LoginWindowLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test
{
    using Data.Model;
    using Data.Repository;
    using Logic.Logics;
    using NUnit.Framework;

    /// <summary>
    /// The LoginWindowLogic's tests
    /// </summary>
    [TestFixture]
    public class LoginWindowLogicTests
    {
        private LoginWindowLogic logic;
        private Repository<User> userRepository;

        /// <summary>
        /// Sets up the data for the tests
        /// </summary>
        [SetUp]
        public void Main()
        {
            Database db = new Database();
            this.userRepository = new Repository<User>(db);
            this.logic = new LoginWindowLogic(this.userRepository);
        }

        /// <summary>
        /// It tests that the IsUserExistsInTheDatabaseTest works well.
        /// </summary>
        /// <param name="username">The user's username</param>
        /// <param name="password">The user's password</param>
        [TestCase(null, null)]
        [TestCase("cat", "dog")]
        [TestCase("DROP TABLE", "MY FRIEND")]
        public void TestThatFalseUserExistsInTheDatabase(string username, string password)
        {
            Assert.That(this.logic.IsUserExistsInTheDatabase(username, password), Is.False);
        }
    }
}
