﻿// <copyright file="IContractLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test
{
    using System;
    using Data;
    using Data.Model;
    using Logic;
    using Logic.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The IConatract Logic's tests
    /// </summary>
    [TestFixture]
  public class IContractLogicTests
    {
        private Mock<IContractLogic> contractLogicMock;
        private Mock<IRepository<Contract>> repositoryMock;
        private Contract[] testModels;

        /// <summary>
        /// Sets up the moq for tests.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.testModels = new[]
            {
                new Contract() { id = 1, date_valid_from = DateTime.Now, date_valid_to = DateTime.Now.AddDays(10) },
                new Contract() { id = 2, date_valid_from = DateTime.Now, date_valid_to = DateTime.Now.AddDays(10) }
            };

            this.contractLogicMock = new Mock<IContractLogic>();
            this.repositoryMock = new Mock<IRepository<Contract>>();
        }

        /// <summary>
        /// Tests that when adding new contract forwards to repository.
        /// </summary>
        [TestCase]
        public void TestWhenTestThatWhenAddingNewContractForwardsToRepository()
        {
            // ARRANGE
            this.repositoryMock.Setup(m => m.Remove(It.IsAny<Contract>()));
            var contractWindowLogic = new ContractWindowLogic(this.repositoryMock.Object, null);

            // ACT
            contractWindowLogic.Add(this.testModels[0]);

            // ASSERT
            this.repositoryMock.Verify(m => m.Add(this.testModels[0]), Times.Once);
        }
    }
}
