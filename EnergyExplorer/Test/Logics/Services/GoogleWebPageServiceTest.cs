﻿// <copyright file="GoogleWebPageServiceTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test.Logics.Services
{
    using Data.Model;
    using Logic.Services;
    using NUnit.Framework;

    [TestFixture]
    public class GoogleWebPageServiceTest
    {
        public GoogleWebPageService Service { get; set; }

        public Meter Meter { get; set; }

        [SetUp]
        public void Main()
        {
            this.Meter = new Meter() { longitude = 47.2989391, latitude = 19.4544675 };
            this.Service = new GoogleWebPageService();
        }

        [Test]
        public void TestOpenGoogleMapsWithMeterLocationWithNullValues()
        {
            this.Service.OpenGoogleMapsWithMeterLocation(null, 0);
            Assert.That(true, Is.True);
        }

        [Test]
        public void TestOpenGoogleMapsWithMeterLocationWithMainSetUp()
        {
            this.Service.OpenGoogleMapsWithMeterLocation(this.Meter, 10);
            Assert.That(true, Is.True);
        }

        [Test]
        public void TestOpenGoogleMapsWithMeterLocationWithVeryBigZoomNumber()
        {
            this.Service.OpenGoogleMapsWithMeterLocation(this.Meter, 999999);
            Assert.That(true, Is.True);
        }

    }
}
