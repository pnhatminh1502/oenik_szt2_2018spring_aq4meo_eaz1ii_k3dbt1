﻿// <copyright file="IBilllogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test
{
    using System.Linq;
    using Data;
    using Data.Model;
    using Logic.Interfaces;
    using Logic.Logics;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The BillLogic's tests
    /// </summary>
    [TestFixture]
    public class IBilllogicTest
    {
        private Mock<IBillLogic> billLogicMock;
        private Mock<IRepository<Bill>> repositoryMock;
        private Bill[] testModels;

        /// <summary>
        /// Setup for the tests.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.testModels = new[]
            {
                new Bill() { id = 1, comment = "Something" },
                new Bill() { id = 2, comment = "Something" }
            };

            this.billLogicMock = new Mock<IBillLogic>();
            this.repositoryMock = new Mock<IRepository<Bill>>();
        }

        /// <summary>
        /// Tests that the repository size is equal to what logic get all returns.
        /// </summary>
        [Test]
        public void TestThatRepositorySizeIsEqualToWhatLogicGetAllReturns()
        {
            // Setup for the function that returns fake data (the test models)
            this.repositoryMock.Setup(a => a.GetAll()).Returns(this.testModels);

            BillWindowLogic logic = new BillWindowLogic(this.repositoryMock.Object, null);

            Assert.That(logic.GetAll(), Is.EqualTo(this.repositoryMock.Object.GetAll()));
        }

        /// <summary>
        /// Tests the that logic get by identifier is returns the same as repository get by identifier.
        /// </summary>
        [Test]
        public void TestThatLogicGetByIdIsReturnsTheSameAsRepositoryGetById()
        {
            this.repositoryMock.Setup(a => a.GetById(1)).Returns(this.testModels.First(item => item.id == 1));
            BillWindowLogic logic = new BillWindowLogic(this.repositoryMock.Object, null);

            Assert.That(logic.GetById(1).id, Is.EqualTo(this.repositoryMock.Object.GetById(1).id));
        }

        /// <summary>
        /// Tests that when calling remove forwards to repository.
        /// </summary>
        [Test]
        public void TestThatWhenCallingRemoveForwardsToRepository()
        {
            // ARRANGE
            this.repositoryMock.Setup(m => m.Remove(It.IsAny<Bill>()));
            var billWindowLogic = new BillWindowLogic(this.repositoryMock.Object, null);

            // ACT
            billWindowLogic.Remove(this.testModels[0]);

            // ASSERT
            this.repositoryMock.Verify(m => m.Remove(this.testModels[0]), Times.Once);
        }

        /// <summary>
        /// Tests that when calling remove on list change event is fired.
        /// </summary>
        [Test]
        public void TestThatWhenCallingRemoveOnListChangeEventIsFired()
        {
            // ARRANGE
            int calls = 0;
            var billWindowLogic = new BillWindowLogic(this.repositoryMock.Object, null);
            billWindowLogic.OnListChanged += (s, e) => calls++;

            // ACT
            billWindowLogic.Remove(this.testModels[0]);

            // ASSERT
            Assert.That(calls, Is.EqualTo(1));
        }
    }
}
