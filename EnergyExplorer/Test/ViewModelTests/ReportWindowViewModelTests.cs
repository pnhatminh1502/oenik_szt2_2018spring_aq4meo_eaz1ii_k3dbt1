﻿// <copyright file="ReportWindowViewModelTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test.ViewModelTests
{
    using System.Collections.Generic;
    using Data.Model;
    using EnergyExplorer.ViewModels;
    using Logic.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The ReportWindowViewModelTests's Tests
    /// </summary>
    [TestFixture]
    public class ReportWindowViewModelTests
    {
        private Mock<IReportLogic> reportLogic;
        private ReportWindowViewModel reportViewModel;
        private List<Meter> testReports;

        /// <summary>
        /// Setups this instance for the tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.testReports = new List<Meter>();
            this.testReports.Add(new Meter { id = 1 });
            this.reportLogic = new Mock<IReportLogic>();
            this.reportLogic.Setup((q) => q.GetAllMeters()).Returns(this.testReports);
            this.reportViewModel = new ReportWindowViewModel(this.reportLogic.Object);
        }

        /// <summary>
        /// Tests that the report view model returns the valid list.
        /// </summary>
        [Test]
        public void TestThatThereportViewModelReturnsTheValidList()
        {
            Assert.That(this.reportViewModel.Meters.Count, Is.EqualTo(this.testReports.Count));
        }
    }
}
