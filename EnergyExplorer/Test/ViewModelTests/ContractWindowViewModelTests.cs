﻿// <copyright file="ContractWindowViewModelTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test.ViewModelTests
{
    using System.Collections.Generic;
    using Data.Model;
    using EnergyExplorer.ViewModels;
    using Logic.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The COntractWindowViewModel's Tests
    /// </summary>
    [TestFixture]
  public class ContractWindowViewModelTests
    {
        private Mock<IContractLogic> contractLogic;
        private ContractWindowViewModel contractViewModel;
        private List<Contract> testContracts;

        /// <summary>
        /// Setups this instance for the tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.testContracts = new List<Contract>();
            this.testContracts.Add(new Contract { id = 1 });
            this.contractLogic = new Mock<IContractLogic>();
            this.contractLogic.Setup((q) => q.GetAll()).Returns(this.testContracts);
            this.contractViewModel = new ContractWindowViewModel(this.contractLogic.Object);
        }

        /// <summary>
        /// Tests that the contract view model returns the valid list.
        /// </summary>
        [Test]
        public void TestThatTheContractViewModelReturnsTheValidList()
        {
            Assert.That(this.contractViewModel.Contracts.Count, Is.EqualTo(this.testContracts.Count));
        }
    }
}
