﻿// <copyright file="BillWindowViewModelTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test.ViewModelTests
{
    using System.Collections.Generic;
    using Data.Model;
    using EnergyExplorer.ViewModels;
    using Logic.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The billWindowViewModel's Tests
    /// </summary>
    [TestFixture]
    public class BillWindowViewModelTests
    {
        private Mock<IBillLogic> billLogic;
        private BillWindowViewModel billViewModel;
        private List<Bill> testbills;

        /// <summary>
        /// Setups this instance for the tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.testbills = new List<Bill>();
            this.testbills.Add(new Bill { id = 1 });
            this.billLogic = new Mock<IBillLogic>();
            this.billLogic.Setup((q) => q.GetAll()).Returns(this.testbills);
            this.billViewModel = new BillWindowViewModel(this.billLogic.Object);
        }

        /// <summary>
        /// Tests that the bill view model returns the valid list.
        /// </summary>
        [Test]
        public void TestThatTheBillViewModelReturnsTheValidList()
        {
            Assert.That(this.billViewModel.Bills.Count, Is.EqualTo(this.testbills.Count));
        }
    }
}
