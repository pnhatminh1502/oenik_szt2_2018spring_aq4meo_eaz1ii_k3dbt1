﻿// <copyright file="EPermissionLevel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Enums
{
    /// <summary>
    /// The permission level
    /// </summary>
    public enum EPermissionLevel
    {
        /// <summary>
        /// The guest
        /// </summary>
        Guest,

        /// <summary>
        /// The user
        /// </summary>
        User,

        /// <summary>
        /// The admin
        /// </summary>
        Admin
    }
}
