﻿// <copyright file="IPermission.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using Logic.Enums;

    /// <summary>
    /// The IPermission interface
    /// </summary>
    public interface IPermission
    {
        /// <summary>
        /// Checks that the useer has the permission to execute a function
        /// </summary>
        /// <param name="userid">The user id itself</param>
        /// <param name="requiredPermissionLevel">The permission level what user must have</param>
        /// <returns>Returns true if the user has the permission</returns>
        bool UserHasThePermissionToExecuteFunction(int userid, EPermissionLevel requiredPermissionLevel);
    }
}
