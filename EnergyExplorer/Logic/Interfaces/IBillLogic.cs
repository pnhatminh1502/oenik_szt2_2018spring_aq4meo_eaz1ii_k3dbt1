﻿// <copyright file="IBillLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using System.Collections.ObjectModel;
    using Data;
    using Data.Model;

    /// <summary>
    /// The IBillogic interface
    /// </summary>
    /// <seealso cref="Logic.Interfaces.IPermission" />
    /// <seealso cref="Data.IDataManipulation{Bill}" />
    public interface IBillLogic : IPermission, IDataManipulation<Bill>
    {
        /// <summary>
        /// Occurs when [on list changed].
        /// </summary>
        event EventHandler OnListChanged;
    }
}
