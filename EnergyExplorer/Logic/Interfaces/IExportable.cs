﻿// <copyright file="IExportable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using Data.Model;

    /// <summary>
    /// The IExportable interface
    /// </summary>
    public interface IExportable
    {
        /// <summary>
        /// Exports to PDF.
        /// </summary>
        /// <param name="meter">The meter.</param>
        /// <param name="from">The start period.</param>
        /// <param name="to">The end period.</param>
        void ExportToPdf(Meter meter, DateTime from, DateTime to);
    }
}
