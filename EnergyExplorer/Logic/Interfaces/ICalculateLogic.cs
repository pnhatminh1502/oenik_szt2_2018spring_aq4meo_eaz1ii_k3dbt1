﻿// <copyright file="ICalculateLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using Data.Model;

    /// <summary>
    /// The ICalculateLogic interface
    /// </summary>
    public interface ICalculateLogic
    {
        /// <summary>
        /// Calculates the consumption.
        /// </summary>
        /// <param name="energyType">Type of the energy.</param>
        /// <param name="date_from">The date from.</param>
        /// <param name="date_to">The date to.</param>
        /// <returns> The sum of the consumption</returns>
        float CalculateConsumption(EnergyType energyType, DateTime date_from, DateTime date_to);
    }
}
