﻿// <copyright file="IReportLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Data.Model;
    using LiveCharts;

    /// <summary>
    /// The IReportLogic interface
    /// </summary>
    /// <seealso cref="Logic.Interfaces.IPermission" />
    public interface IReportLogic : IPermission
    {
        /// <summary>
        /// Returns all of the meters from the database
        /// </summary>
        /// <returns>List of meters</returns>
        IList<Meter> GetAllMeters();

        /// <summary>
        /// Gets the 15 minutes datas from database according to the given period
        /// </summary>
        /// <param name="from">DateTime from</param>
        /// <param name="to">DateTime to</param>
        /// <param name="meterID">The meter's id</param>
        /// <returns>List of Meter15m</returns>
        IList<Meter15m> Get15MData(DateTime from, DateTime to, int meterID);
        SeriesCollection GenerateMeter15mSeriesForReportDiagram(DateTime dateFrom, DateTime dateTo, int id);
        void ShowMeterLocationInGoogleMaps(Meter selectedMeter);
    }
}
