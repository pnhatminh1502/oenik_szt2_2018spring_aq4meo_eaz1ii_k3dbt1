﻿// <copyright file="IContractLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using System.Collections.ObjectModel;
    using Data;
    using Data.Model;

    /// <summary>
    /// The IContractLogic interface
    /// </summary>
    /// <seealso cref="Logic.Interfaces.IPermission" />
    /// <seealso cref="Data.IDataManipulation{Contract}" />
    public interface IContractLogic : IPermission, IDataManipulation<Contract>
    {
        /// <summary>
        /// Occurs when [on list changed].
        /// </summary>
        event EventHandler OnListChanged;
    }
}
