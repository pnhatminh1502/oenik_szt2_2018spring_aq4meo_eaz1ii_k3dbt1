﻿// <copyright file="IAuthentication.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    /// <summary>
    /// The IAuthentication interface
    /// </summary>
    public interface IAuthentication
    {
        /// <summary>
        /// Determines whether [is user exists in the database] [the specified username].
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns>
        ///   <c>true</c> if [is user exists in the database] [the specified username]; otherwise, <c>false</c>.
        /// </returns>
        bool IsUserExistsInTheDatabase(string username, string password);
    }
}
