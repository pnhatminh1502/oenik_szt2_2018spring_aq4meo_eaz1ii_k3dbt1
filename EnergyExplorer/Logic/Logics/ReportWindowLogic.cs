﻿// <copyright file="ReportWindowLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;
    using Data;
    using Data.Model;
    using Data.Repository;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using LiveCharts;
    using LiveCharts.Wpf;
    using Logic.Enums;
    using Logic.Interfaces;
    using Services;

    /// <summary>
    /// The ReportWindow's logic
    /// </summary>
    /// <seealso cref="Logic.Interfaces.IReportLogic" />
    /// <seealso cref="Logic.Interfaces.IExportable" />
    public class ReportWindowLogic : IReportLogic, IExportable
    {
        private readonly IRepository<Meter> meterRepository;
        private readonly IRepository<Meter15m> meter15mRepository;
        private readonly IRepository<EnergyType> energyTypeRepository;
        private readonly IRepository<Tariff> tariffRepository;
        private readonly IRepository<User> userRepository;
        private GoogleWebPageService googleWebPageService = new GoogleWebPageService();

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportWindowLogic"/> class.
        /// </summary>
        /// <param name="meterRepository">The meter repository.</param>
        /// <param name="meter15mRepository">The meter15m repository.</param>
        /// <param name="userRepository">The user repositor.</param>
        /// <param name="energyTypeRepository">EnerygyType Repository</param>
        /// <param name="tariffRepository">Tariff Repostiory</param>
        public ReportWindowLogic(IRepository<Meter> meterRepository, IRepository<Meter15m> meter15mRepository, Repository<EnergyType> energyTypeRepository, Repository<Tariff> tariffRepository, IRepository<User> userRepository)
        {
            this.meterRepository = meterRepository;
            this.meter15mRepository = meter15mRepository;
            this.tariffRepository = tariffRepository;
            this.energyTypeRepository = energyTypeRepository;
            this.userRepository = userRepository;
        }

        /// <summary>
        /// Gets all of the meters from the database
        /// </summary>
        /// <returns>List of meter</returns>
        public IList<Meter> GetAllMeters() => this.meterRepository.GetAll();

        /// <summary>
        /// Get Meter15 data of a meter type within a period of time
        /// </summary>
        /// <param name="from">Start of period</param>
        /// <param name="to">End of period</param>
        /// <param name="meterID">ID of meter type</param>
        /// <returns>List of Meter15M</returns>
        public IList<Meter15m> Get15MData(DateTime from, DateTime to, int meterID)
        {
            return this.meter15mRepository.GetAll().Where(item =>
                    item.date_from >= from &&
                    item.date_to <= to &&
                    item.meter_id == meterID).ToList<Meter15m>();
        }

        /// <summary>
        /// Open a new browser and show the exact location of the meter in the world
        /// </summary>
        /// <param name="meter">The meter which we want to see</param>
        public void ShowMeterLocationInGoogleMaps(Meter meter)
        {
            this.googleWebPageService.OpenGoogleMapsWithMeterLocation(meter, 5);
        }

        /// <summary>
        /// Generate a Line series of the Meter15m for the Report's Diagram function
        /// </summary>
        /// <param name="from">Start of period</param>
        /// <param name="to">End of period</param>
        /// <param name="meterID">ID of meter type</param>
        /// <returns>A Line Series Collection</returns>
        public SeriesCollection GenerateMeter15mSeriesForReportDiagram(DateTime from, DateTime to, int meterID)
        {
            IList<Meter15m> meter15MData = this.Get15MData(from, to, meterID);
            ChartValues<double> meter15MValues = this.ChartValuesForReportDiagram(meter15MData);
            SeriesCollection meter15mSeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Meter 15 minute",
                    Values = meter15MValues
                }
            };

            return meter15mSeriesCollection;
        }

        /// <summary>
        /// This function generates a pdf according a meter and it's meter15m data.
        /// </summary>
        /// <param name="meter">The context what will be exported</param>
        /// <param name="from">Start period of time to be exported</param>
        /// <param name="to">End period of time to be exported</param>
        public void ExportToPdf(Meter meter, DateTime from, DateTime to)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "PDF File (.pdf)|*.pdf";
            DialogResult dialogResult = saveFileDialog.ShowDialog();
            float totalConsumption = this.ConsumptionCalculation(meter, from, to);
            string eneryTypeName = this.GetEnergyType(meter).name;
            IEnumerable<Tariff> tariffs = this.GetTariffs(meter, from, to);

            if (dialogResult == System.Windows.Forms.DialogResult.OK)
            {
                Document document = new Document();
                FileStream fs = File.Create(saveFileDialog.FileName);
                PdfWriter.GetInstance(document, fs);
                document.Open();
                document.Add(new Paragraph($"Report of the {meter.name} from {from.ToString()} to {to.ToString()}"));
                document.Add(new Paragraph($"Total consumption: {totalConsumption}."));
                document.Add(new Paragraph($"Energy Type: {eneryTypeName}."));
                document.Add(new Paragraph($"----------------------------------------------"));
                document.Add(new Paragraph("Tariffs report"));
                if (tariffs.Count() > 0)
                {
                    foreach (var tariff in tariffs)
                    {
                        document.Add(new Paragraph($"Unit: {tariff.unit}."));
                        document.Add(new Paragraph($"Value: {tariff.value}."));
                        document.Add(new Paragraph($"From: {tariff.date_from}."));
                        document.Add(new Paragraph($"To: {tariff.date_to}."));
                        document.Add(new Paragraph($"----------------------------------------------"));
                    }
                }
                else
                {
                    document.Add(new Paragraph("There is no tariff for this report"));
                    document.Add(new Paragraph($"----------------------------------------------"));
                }

                document.Close();
            }
        }

        /// <summary>
        /// Checks that the useer has the permission to execute a function
        /// </summary>
        /// <param name="userid">The user id itself</param>
        /// <param name="requiredPermissionLevel">The permission level what user must have</param>
        /// <returns>
        /// Returns true if the user has the permission
        /// </returns>
        public bool UserHasThePermissionToExecuteFunction(int userid, EPermissionLevel requiredPermissionLevel)
        {
            User user = this.userRepository.GetById(userid);
            if (user != null)
            {
                return this.userRepository.GetById(userid).permission_lvl == (int)requiredPermissionLevel;
            }

            return false;
        }

        private ChartValues<double> ChartValuesForReportDiagram(IList<Meter15m> meter15MData)
        {
            ChartValues<double> cv = new ChartValues<double>();
            if (meter15MData != null)
            {
                List<double> tempValues = new List<double>();
                foreach (var item in meter15MData)
                {
                    if (item.value != null)
                    {
                        tempValues.Add(item.value.Value);
                    }
                }

                cv.AddRange(tempValues);
                return cv;
            }

            return null;
        }

        private float ConsumptionCalculation(Meter meter, DateTime from, DateTime to)
        {
            float sum = 0;
            IList<Meter15m> meter15MData = this.Get15MData(from, to, meter.id);
            foreach (Meter15m m15 in meter15MData)
            {
                sum += (float)m15.value;
            }

            return sum;
        }

        private EnergyType GetEnergyType(Meter meter)
        {
            return this.energyTypeRepository.GetById(meter.energy_type_id);
        }

        private IList<Tariff> GetTariffs(Meter meter, DateTime from, DateTime to)
        {
            return this.tariffRepository.GetAll().Where(item =>
                    item.energy_type_id == meter.energy_type_id &&
                    item.date_from >= from &&
                    item.date_to <= to).ToList();
        }
    }
}
