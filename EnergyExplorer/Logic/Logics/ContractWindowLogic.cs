﻿// <copyright file="ContractWindowLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Linq.Expressions;
    using Data;
    using Data.Model;
    using Enums;
    using Interfaces;

    /// <summary>
    /// The Contract Window's logic
    /// </summary>
    /// <seealso cref="Logic.Interfaces.IContractLogic" />
    public class ContractWindowLogic : IContractLogic
    {
        private readonly IRepository<Contract> contractRepository;
        private readonly IRepository<User> userRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractWindowLogic"/> class.
        /// </summary>
        /// <param name="contractRepository">The contract repository.</param>
        /// <param name="userRepository">The user repository.</param>
        public ContractWindowLogic(IRepository<Contract> contractRepository, IRepository<User> userRepository)
        {
            this.contractRepository = contractRepository;
            this.userRepository = userRepository;
        }

        /// <summary>
        /// Occurs when [on list changed].
        /// </summary>
        public event EventHandler OnListChanged;

        /// <summary>
        /// Adds the specified contract.
        /// </summary>
        /// <param name="contract">The contract.</param>
        public void Add(Contract contract)
        {
            if (contract != null)
            {
                try
                {
                    this.contractRepository.Add(contract);
                }
                finally
                {
                    this.OnModelListChanged();
                }
            }
        }

        /// <summary>
        /// Edits the specified contract.
        /// </summary>
        /// <param name="contract">The contract.</param>
        public void Edit(Contract contract)
        {
            if (contract != null)
            {
                this.contractRepository.Edit(contract);
            }
        }

        /// <summary>
        /// Find T elements according lambda expression
        /// </summary>
        /// <param name="predicate">The lambda expression for filtering</param>
        /// <returns>
        /// Return list of T item
        /// </returns>
        public IList<Contract> Find(Expression<Func<Contract, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get all of the T object from the database
        /// </summary>
        /// <returns>
        /// The list of the T items
        /// </returns>
        public IList<Contract> GetAll()
        {
            return this.contractRepository.GetAll();
        }

        /// <summary>
        /// Return the object if exists in the database using id.
        /// </summary>
        /// <param name="id">the id of the object</param>
        /// <returns>
        /// the T object
        /// </returns>
        public Contract GetById(int id)
        {
            return this.contractRepository.GetById(id);
        }

        /// <summary>
        /// Removes the specified contract. If the user has permission.
        /// </summary>
        /// <param name="contract">The contract.</param>
        public void Remove(Contract contract)
        {
            if (contract != null)
            {
                try
                {
                    this.contractRepository.Remove(contract);
                }
                finally
                {
                    this.OnModelListChanged();
                }
            }
        }

        /// <summary>
        /// Checks that the user has the permission to execute a function
        /// </summary>
        /// <param name="userid">The user id itself</param>
        /// <param name="requiredPermissionLevel">The permission level what user must have</param>
        /// <returns>
        /// Returns true if the user has the permission
        /// </returns>
        public bool UserHasThePermissionToExecuteFunction(int userid, EPermissionLevel requiredPermissionLevel)
        {
            User user = this.userRepository.GetById(userid);
            if (user != null)
            {
                return this.userRepository.GetById(userid).permission_lvl == (int)requiredPermissionLevel;
            }

            return false;
        }

        private void OnModelListChanged()
        {
            this.OnListChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
