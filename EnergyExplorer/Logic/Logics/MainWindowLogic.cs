﻿// <copyright file="MainWindowLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Logics
{
    using System.Collections.Generic;
    using System.Linq;
    using Data.Model;
    using Data.Repository;
    using LiveCharts;
    using LiveCharts.Wpf;

    public class MainWindowLogic
    {
        private readonly Repository<Tariff> tariffRepository;
        private readonly Repository<EnergyType> energyTypeRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowLogic"/> class.
        /// </summary>
        /// <param name="tariffRepository">Tariff Repository</param>
        /// <param name="energyTypeRepository">EnergyType Repository</param>
        public MainWindowLogic(Repository<Tariff> tariffRepository, Repository<EnergyType> energyTypeRepository)
        {
            this.energyTypeRepository = energyTypeRepository;
            this.tariffRepository = tariffRepository;
        }

        /// <summary>
        /// Generate Tariffs SeriesCollection By Energy Type for Diagram
        /// </summary>
        /// <returns>SeriesColelction</returns>
        public SeriesCollection GenerateTariffsReportByEnergyTypeDiagram()
        {
            IList<EnergyType> energyTypes = this.energyTypeRepository.GetAll().ToList();
            SeriesCollection series = new SeriesCollection();
            foreach (var energyType in energyTypes)
            {
                series.Add(new LineSeries
                {
                    Title = energyType.name,
                    Values = this.ChartValuesForReportDiagram(energyType.id)
                });
            }

            return series;
        }

        private ChartValues<double> ChartValuesForReportDiagram(int energyTypeID)
        {
            ChartValues<double> cv = new ChartValues<double>();
            IList<Tariff> tariffs = this.GetTariffsByType(energyTypeID);
            if (tariffs != null)
            {
                List<double> tempValues = new List<double>();
                foreach (var item in tariffs)
                {
                    tempValues.Add((double)item.value);
                }

                cv.AddRange(tempValues);
                return cv;
            }

            return null;
        }

        private IList<Tariff> GetTariffsByType(int energyTypeID)
        {
            return this.tariffRepository.GetAll().Where(item => item.energy_type_id == energyTypeID).ToList();
        }
    }
}
