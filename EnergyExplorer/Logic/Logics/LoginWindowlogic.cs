﻿// <copyright file="LoginWindowLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Logics
{
    using System.Collections.Generic;
    using System.Linq;
    using Data;
    using Data.Model;
    using Logic.Interfaces;

    /// <summary>
    /// The LoginWindow's logic
    /// </summary>
    /// <seealso cref="Logic.Interfaces.IAuthentication" />
    public class LoginWindowLogic : IAuthentication
    {
        private readonly IRepository<User> userRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginWindowLogic"/> class.
        /// </summary>
        /// <param name="userRepository">The user repository.</param>
        public LoginWindowLogic(IRepository<User> userRepository)
        {
            this.userRepository = userRepository;
        }

        /// <summary>
        /// Check that user is exists in the database
        /// </summary>
        /// <param name="username">The user's username</param>
        /// <param name="password">The user's password</param>
        /// <returns>Return true if exists</returns>
        public bool IsUserExistsInTheDatabase(string username, string password)
        {
            // IEnumerable<User> users = this.userRepository.GetAll();
            var result = this.userRepository.Find(item => item.username == username && item.password == password);
            return result != null && result.Count() > 0;
        }
    }
}
