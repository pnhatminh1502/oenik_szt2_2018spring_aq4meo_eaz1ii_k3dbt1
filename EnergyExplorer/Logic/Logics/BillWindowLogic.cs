﻿// <copyright file="BillWindowLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Logics
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq.Expressions;
    using Data;
    using Data.Model;
    using Enums;
    using Interfaces;

    /// <summary>
    /// The BillWindow's logic
    /// </summary>
    /// <seealso cref="Logic.Interfaces.IBillLogic" />
    public class BillWindowLogic : IBillLogic
    {
        private readonly IRepository<Bill> billRepository;
        private readonly IRepository<User> userRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BillWindowLogic"/> class.
        /// </summary>
        /// <param name="billRepository">The bill repository.</param>
        /// <param name="userRepository">The user repository.</param>
        public BillWindowLogic(IRepository<Bill> billRepository, IRepository<User> userRepository)
        {
            this.billRepository = billRepository;
            this.userRepository = userRepository;
        }

        /// <summary>
        /// Occurs when [on list changed].
        /// </summary>
        public event EventHandler OnListChanged;

        /// <summary>
        /// Add a new bill object to the database
        /// </summary>
        /// <param name="bill">The bill object what we want to add to the database</param>
        public void Add(Bill bill)
        {
            if (bill != null)
            {
                try
                {
                    this.billRepository.Add(bill);
                }
                finally
                {
                    this.OnModelListChanged();
                }
            }
        }

        /// <summary>
        /// Removes the specified bill.
        /// </summary>
        /// <param name="bill">The bill.</param>
        public void Remove(Bill bill)
        {
            if (bill != null)
            {
                try
                {
                    this.billRepository.Remove(bill);
                }
                finally
                {
                    this.OnModelListChanged();
                }
            }
        }

        /// <summary>
        /// Get all of the T object from the database
        /// </summary>
        /// <returns>
        /// The list of the T items
        /// </returns>
        public IList<Bill> GetAll()
        {
            return this.billRepository.GetAll();
        }

        /// <summary>
        /// Edits the specified bill.
        /// </summary>
        /// <param name="bill">The bill.</param>
        public void Edit(Bill bill)
        {
            if (bill != null)
            {
                this.billRepository.Edit(bill);
            }
        }

        /// <summary>
        /// Find T elements according lambda expression
        /// </summary>
        /// <param name="predicate">The lambda expression for filtering</param>
        /// <returns>
        /// Return list of T item
        /// </returns>
        public IList<Bill> Find(Expression<Func<Bill, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Return the object if exists in the database using id.
        /// </summary>
        /// <param name="id">the id of the object</param>
        /// <returns>
        /// the T object
        /// </returns>
        public Bill GetById(int id)
        {
            return this.billRepository.GetById(id);
        }

        /// <summary>
        /// Checks that the useer has the permission to execute a function
        /// </summary>
        /// <param name="userid">The user id itself</param>
        /// <param name="requiredPermissionLevel">The permission level what user must have</param>
        /// <returns>
        /// Returns true if the user has the permission
        /// </returns>
        public bool UserHasThePermissionToExecuteFunction(int userid, EPermissionLevel requiredPermissionLevel)
        {
            User user = this.userRepository.GetById(userid);
            if (user != null)
            {
                return this.userRepository.GetById(userid).permission_lvl == (int)requiredPermissionLevel;
            }

            return false;
        }

        /// <summary>
        /// It invokes the Event in the BillWindowViewModel.
        /// </summary>
        private void OnModelListChanged()
        {
            this.OnListChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
