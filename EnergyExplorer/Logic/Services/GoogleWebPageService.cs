﻿// <copyright file="GoogleWebPageService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Services
{
    using System;
    using System.Diagnostics;
    using System.Text;
    using System.Threading;
    using Data.Model;

    public class GoogleWebPageService
    {
        public GoogleWebPageService()
        {
            this.Url = new Uri("https://www.google.com/maps/place/");
        }

        public GoogleWebPageService(Uri uri)
        {
            this.Url = uri;
        }

        public Uri Url { get; set; }

        /// <summary>
        /// It opens a new google map windows using the meter longitude and latitude.
        /// </summary>
        /// <param name="meter">The meter</param>
        /// <param name="zoomSize">Size of the zoom in the map</param>
        public void OpenGoogleMapsWithMeterLocation(Meter meter, int zoomSize)
        {
            if (meter != null)
            {
                using (Process process = new Process())
                {
                    StringBuilder fragmentBuilder = new StringBuilder();
                    fragmentBuilder.Append(this.Url.ToString());
                    fragmentBuilder.AppendFormat("@{0},{1},{2}", meter.longitude, meter.latitude, zoomSize);
                    process.StartInfo.FileName = fragmentBuilder.ToString();
                    process.Start();
                    Thread.Sleep(500);
                }
            }
        }
    }
}
