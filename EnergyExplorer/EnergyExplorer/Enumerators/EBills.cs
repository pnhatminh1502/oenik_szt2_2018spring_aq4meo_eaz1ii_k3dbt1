﻿// <copyright file="EBills.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.Enumerators
{
    /// <summary>
    /// The EBills enum
    /// </summary>
    public enum EBills
    {
        /// <summary>
        /// The energy
        /// </summary>
        Energy,

        /// <summary>
        /// The heat
        /// </summary>
        Heat,

        /// <summary>
        /// The water
        /// </summary>
        Water
    }
}
