﻿// <copyright file="BillEditWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.Windows
{
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using Data;
    using Data.Model;
    using Data.Repository;
    using ViewModels;

    /// <summary>
    /// Interaction logic for BillEditWindow.xaml
    /// </summary>
    public partial class BillEditWindow : Window
    {
        private BillEditorViewModel viewModel;
        private IRepository<Meter> meterRepository;
        private IRepository<Contract> contractRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BillEditWindow"/> class with bill object.
        /// </summary>
        /// <param name="bill">The bill object what we want to edit</param>
        public BillEditWindow(Bill bill)
        {
            if (bill != null)
            {
                this.InitializeComponent();
                this.BillObject = bill;
                this.Title = "Edit bill";
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BillEditWindow"/> class.
        /// </summary>
        public BillEditWindow()
        {
            this.InitializeComponent();
            this.BillObject = new Bill()
            {
                date_from = System.DateTime.Now,
                date_to = System.DateTime.Now
            };
            this.Title = "Add new bill";
        }

        /// <summary>
        /// Gets or Sets the Bill
        /// </summary>
        public Bill BillObject { get; private set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Database db = new Database();
            this.meterRepository = new Repository<Meter>(db);
            this.contractRepository = new Repository<Contract>(db);
            this.viewModel = new BillEditorViewModel(this.meterRepository, this.contractRepository);
            this.viewModel.Bill = this.BillObject;
            this.DataContext = this.viewModel;
        }

        private void OnSaveClick(object sender, RoutedEventArgs e)
        {
            if (this.IsValid(this) == true)
            {
                this.DialogResult = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Something is not valid! Please check it !");
            }
        }

        private void OnCancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private bool IsValid(DependencyObject obj)
        {
            return !Validation.GetHasError(obj) &&
            LogicalTreeHelper.GetChildren(obj)
            .OfType<DependencyObject>()
            .All(this.IsValid);
        }
    }
}
