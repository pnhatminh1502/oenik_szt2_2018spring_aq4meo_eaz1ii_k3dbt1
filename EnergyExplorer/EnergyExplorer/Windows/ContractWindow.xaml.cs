﻿// <copyright file="ContractWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.Windows
{
    using System.Windows;
    using Data.Model;
    using Data.Repository;
    using EnergyExplorer.Enumerators;
    using Logic;
    using Logic.Interfaces;
    using ViewModels;

    /// <summary>
    /// Interaction logic for ContractWindow.xaml
    /// </summary>
    public partial class ContractWindow : Window
    {
        private ContractWindowViewModel viewModel;
        private IContractLogic logic;
        private EContracts uID;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractWindow"/> class.
        /// </summary>
        /// <param name="uID">The u identifier.</param>
        public ContractWindow(EContracts uID)
        {
            this.InitializeComponent();
            this.uID = uID;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Database db = new Database();
            Repository<Contract> contractRepository = new Repository<Contract>(db);
            Repository<User> userRepository = new Repository<User>(db);
            this.logic = new ContractWindowLogic(contractRepository, userRepository);
            this.viewModel = new ContractWindowViewModel(this.logic);
            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// Removes selected contract from the datagrid
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">E routed event</param>
        private void RemoveContractButton(object sender, RoutedEventArgs e)
        {
            if (this.logic.UserHasThePermissionToExecuteFunction(999, Logic.Enums.EPermissionLevel.Admin))
            {
                if (this.viewModel.SelectedContract != null)
                {
                    this.viewModel.Delete(this.viewModel.SelectedContract);
                }
            }
            else
            {
                MessageBox.Show("You dont have permission");
            }
        }

        private void EditContractButton(object sender, RoutedEventArgs e)
        {
            if (this.logic.UserHasThePermissionToExecuteFunction(1, Logic.Enums.EPermissionLevel.User))
            {
                if (this.viewModel.SelectedContract != null)
                {
                    ContractEditWindow editWindow = new ContractEditWindow(this.viewModel.SelectedContract);
                    if (editWindow.ShowDialog() == true && editWindow.DialogResult == true)
                    {
                        this.viewModel.Edit(this.viewModel.SelectedContract);
                    }
                }
            }
            else
            {
                MessageBox.Show("You dont have permission");
            }
        }

        private void CloseContractWindowButton(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AddNewContractButton(object sender, RoutedEventArgs e)
        {
            if (this.logic.UserHasThePermissionToExecuteFunction(1, Logic.Enums.EPermissionLevel.User))
            {
                ContractEditWindow editWindow = new ContractEditWindow();
                if (editWindow.ShowDialog() == true && editWindow.DialogResult == true)
                {
                    this.viewModel.Add(editWindow.ContractObject);
                }
            }
            else
            {
                MessageBox.Show("You dont have permission");
            }
        }
    }
}
