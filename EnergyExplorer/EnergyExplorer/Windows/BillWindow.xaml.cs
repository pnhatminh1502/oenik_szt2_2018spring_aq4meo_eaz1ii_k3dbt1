﻿// <copyright file="BillWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.Windows.Bills
{
    using System.Windows;
    using System.Windows.Controls;
    using Data.Model;
    using Data.Repository;
    using EnergyExplorer.Enumerators;
    using Logic.Interfaces;
    using Logic.Logics;
    using ViewModels;

    /// <summary>
    /// Interaction logic for BillsWindow.xaml
    /// </summary>
    public partial class BillWindow : Window
    {
        private BillWindowViewModel viewModel;
        private IBillLogic logic;
        private EBills uID;

        /// <summary>
        /// Initializes a new instance of the <see cref="BillWindow"/> class.
        /// </summary>
        /// <param name="uID">The u identifier.</param>
        public BillWindow(EBills uID)
        {
            this.InitializeComponent();
            this.uID = uID;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Database db = new Database();
            Repository<Bill> billRepository = new Repository<Bill>(db);
            Repository<User> userRepository = new Repository<User>(db);
            this.logic = new BillWindowLogic(billRepository, userRepository);
            this.viewModel = new BillWindowViewModel(this.logic);
            this.DataContext = this.viewModel;
        }

        private void RemoveBillButton(object sender, RoutedEventArgs e)
        {
            if (this.logic.UserHasThePermissionToExecuteFunction(1, Logic.Enums.EPermissionLevel.User))
            {
                if (this.viewModel.SelectedBill != null)
                {
                    this.viewModel.Delete(this.viewModel.SelectedBill);
                }
            }
            else
            {
                MessageBox.Show("You dont have permission");
            }
        }

        private void EditBillButton(object sender, RoutedEventArgs e)
        {
            if (this.logic.UserHasThePermissionToExecuteFunction(1, Logic.Enums.EPermissionLevel.User))
            {
                if (this.viewModel.SelectedBill != null)
                {
                    BillEditWindow editWindow = new BillEditWindow(this.viewModel.SelectedBill);
                    if (editWindow.ShowDialog() == true && editWindow.DialogResult == true)
                    {
                        this.viewModel.Edit(this.viewModel.SelectedBill);
                    }
                }
            }
            else
            {
                MessageBox.Show("You dont have permission");
            }
        }

        private void CloseBillWindowButton(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AddNewBillButton(object sender, RoutedEventArgs e)
        {
            if (this.logic.UserHasThePermissionToExecuteFunction(1, Logic.Enums.EPermissionLevel.User))
            {
                BillEditWindow editWindow = new BillEditWindow();
                if (editWindow.ShowDialog() == true && editWindow.DialogResult == true)
                {
                    this.viewModel.Add(editWindow.BillObject);
                }
            }
            else
            {
                MessageBox.Show("You dont have permission");
            }
        }
    }
}