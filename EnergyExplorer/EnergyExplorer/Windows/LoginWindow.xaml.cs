﻿// <copyright file="LoginWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.Windows
{
    using System.Windows;
    using System.Windows.Controls;
    using Data.Model;
    using Data.Repository;
    using Logic.Logics;
    using ViewModels;

    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private LoginWindowViewModel viewModel;
        private LoginWindowLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginWindow"/> class.
        /// </summary>
        public LoginWindow()
        {
            this.InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Database db = new Database();
            Repository<User> userRepository = new Repository<User>(db);
            this.logic = new LoginWindowLogic(userRepository);
            this.viewModel = new LoginWindowViewModel(this.logic);

            // this.viewModel.SomeCollection = this.logic.GetDataFromDatabase(xxxxx)
            // mvvmlight, prism, caliburn
            this.DataContext = this.viewModel;
        }

        private void LoginButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.viewModel.Username != null && this.viewModel.Password != null)
            {
                if (this.logic.IsUserExistsInTheDatabase(this.viewModel.Username, this.viewModel.Password))
                {
                    MainWindow mw = new MainWindow();

                    if (mw != null)
                    {
                        this.Close();
                        mw.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("Username or password is bad");
                }
            }
            else
            {
                MessageBox.Show("Username or password is missing");
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var passwordBox = (sender as PasswordBox).Password;
            this.viewModel.Password = passwordBox;
        }
    }
}
