﻿// <copyright file="ContractEditWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.Windows
{
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using Data;
    using Data.Model;
    using Data.Repository;
    using EnergyExplorer.ViewModels;

    /// <summary>
    /// Interaction logic for ContractEditWindow.xaml
    /// </summary>
    public partial class ContractEditWindow : Window
    {
        private ContractEditorViewModel viewModel;
        private IRepository<Contract> contractRepository;
        private IRepository<EnergyType> energyRepository;
        private IRepository<Company> companyRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractEditWindow"/> class.
        /// </summary>
        /// <param name="contract">The contract.</param>
        public ContractEditWindow(Contract contract)
        {
            if (contract != null)
            {
                this.InitializeComponent();
                this.ContractObject = contract;
                this.Title = "Edit Contract";
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractEditWindow"/> class.
        /// </summary>
        public ContractEditWindow()
        {
            this.InitializeComponent();
            this.ContractObject = new Contract()
            {
                date_valid_from = System.DateTime.Now,
                date_valid_to = System.DateTime.Now.AddHours(1)
            };
                this.Title = "Add new contract";
        }

        /// <summary>
        /// Gets the contract object.
        /// </summary>
        /// <value>
        /// The contract object.
        /// </value>
        public Contract ContractObject { get; private set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Database db = new Database();
            this.contractRepository = new Repository<Contract>(db);
            this.energyRepository = new Repository<EnergyType>(db);
            this.companyRepository = new Repository<Company>(db);
            this.viewModel = new ContractEditorViewModel(this.contractRepository, this.energyRepository, this.companyRepository);
            this.viewModel.Contract = this.ContractObject;
            this.DataContext = this.viewModel;
        }

        private void OnSaveClick(object sender, RoutedEventArgs e)
        {
            if (this.IsValid(this) == true)
            {
                this.DialogResult = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Something is not valid! Please check it !");
            }
        }

        private void OnCancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private bool IsValid(DependencyObject obj)
        {
            return !Validation.GetHasError(obj) &&
            LogicalTreeHelper.GetChildren(obj)
            .OfType<DependencyObject>()
            .All(this.IsValid);
        }
    }
}
