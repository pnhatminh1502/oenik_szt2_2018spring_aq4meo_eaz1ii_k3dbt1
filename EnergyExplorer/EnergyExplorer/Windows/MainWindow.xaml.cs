﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer
{
    using System.Windows;
    using System.Windows.Controls;
    using Data.Model;
    using Data.Repository;
    using EnergyExplorer.Windows.Bills;
    using Enumerators;
    using Logic.Logics;
    using ViewModels;
    using Windows;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowLogic logic;
        private MainWindowViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Database db = new Database();
            Repository<Tariff> tariffRepository = new Repository<Tariff>(db);
            Repository<EnergyType> energyTypeRepository = new Repository<EnergyType>(db);
            this.logic = new MainWindowLogic(tariffRepository, energyTypeRepository);
            this.viewModel = new MainWindowViewModel(this.logic);
            this.viewModel.TotalConsumptionSeriesCollection = this.logic.GenerateTariffsReportByEnergyTypeDiagram();
            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// It shows the Report window
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">e RoutedEvent</param>
        private void ReportButtonClick(object sender, RoutedEventArgs e)
        {
            ReportWindow win = new ReportWindow();
            if (win != null)
            {
                win.ShowDialog();
            }
        }

        /// <summary>
        /// It shows the Setting window
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">e RoutedEvent</param>
        private void SettingButtonClick(object sender, RoutedEventArgs e)
        {
            SettingsWindow win = new SettingsWindow();
            if (win != null)
            {
                win.ShowDialog();
            }
        }

        /// <summary>
        /// It shows the Utilities Window
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">e RoutedEvent</param>
        private void UtilitiesBillButtonClick(object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// It shows the Energy Bill window
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">e RoutedEvent</param>
        private void EnergyBillButtonClick(object sender, RoutedEventArgs e)
        {
            BillWindow win = new BillWindow(EBills.Energy);
            if (win != null)
            {
                win.ShowDialog();
            }
        }

        /// <summary>
        /// It shows the Heat Bill window
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">e RoutedEvent</param>
        private void HeatBillButtonClick(object sender, RoutedEventArgs e)
        {
            BillWindow win = new BillWindow(EBills.Heat);
            if (win != null)
            {
                win.ShowDialog();
            }
        }

        /// <summary>
        /// It shows the Water Bill window
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">e RoutedEvent</param>
        private void WaterBillButtonClick(object sender, RoutedEventArgs e)
        {
            BillWindow win = new BillWindow(EBills.Water);
            if (win != null)
            {
                win.ShowDialog();
            }
        }

        /// <summary>
        /// It shows the Energy Contract window
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">e RoutedEvent</param>
        private void EnergyContractButtonClick(object sender, RoutedEventArgs e)
        {
            ContractWindow win = new ContractWindow(EContracts.Energy);
            if (win != null)
            {
                win.ShowDialog();
            }
        }

        /// <summary>
        /// It shows the Heat Contract window
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">e RoutedEvent</param>
        private void HeatContractButtonClick(object sender, RoutedEventArgs e)
        {
            ContractWindow win = new ContractWindow(EContracts.Heat);
            if (win != null)
            {
                win.ShowDialog();
            }
        }

        /// <summary>
        /// It shows the Water Contract window
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">e RoutedEvent</param>
        private void WaterContractButtonClick(object sender, RoutedEventArgs e)
        {
            ContractWindow win = new ContractWindow(EContracts.Water);
            if (win != null)
            {
                win.ShowDialog();
            }
        }
    }
}