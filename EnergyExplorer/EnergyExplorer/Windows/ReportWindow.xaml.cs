﻿// <copyright file="ReportWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.Windows
{
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using Data.Model;
    using Data.Repository;
    using Logic;
    using ViewModels;

    /// <summary>
    /// Interaction logic for ReportWindow.xaml
    /// </summary>
    public partial class ReportWindow : Window
    {
        private ReportWindowViewModel viewModel;
        private ReportWindowLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportWindow"/> class.
        /// </summary>
        public ReportWindow()
        {
            this.InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Database db = new Database();
            Repository<Meter> meterRepository = new Repository<Meter>(db);
            Repository<Meter15m> meter15mRepository = new Repository<Meter15m>(db);
            Repository<Tariff> tariffRepository = new Repository<Tariff>(db);
            Repository<EnergyType> energyTypeRepository = new Repository<EnergyType>(db);
            Repository<User> userRepository = new Repository<User>(db);
            this.logic = new ReportWindowLogic(meterRepository, meter15mRepository, energyTypeRepository, tariffRepository, userRepository);
            this.viewModel = new ReportWindowViewModel(this.logic);
            this.DataContext = this.viewModel;
        }

        private void RefreshButton(object sender, RoutedEventArgs e)
        {
            if (this.logic.UserHasThePermissionToExecuteFunction(1, Logic.Enums.EPermissionLevel.User))
            {
                if (this.viewModel.DateFrom != null && this.viewModel.DateTo != null)
                {
                    this.viewModel.GenerateMeter15mSeriesCollection();
                }
            }
            else
            {
                MessageBox.Show("You dont have permission");
            }
        }

        private void ShowLocationButton(object sender, RoutedEventArgs e)
        {
            if (this.logic.UserHasThePermissionToExecuteFunction(1, Logic.Enums.EPermissionLevel.User))
            {
                if (this.viewModel.SelectedMeter != null)
                {
                    this.logic.ShowMeterLocationInGoogleMaps(this.viewModel.SelectedMeter);
                }
            }
            else
            {
                MessageBox.Show("You dont have permission");
            }
        }

        private void ExportButton(object sender, RoutedEventArgs e)
        {
            if (this.logic.UserHasThePermissionToExecuteFunction(1, Logic.Enums.EPermissionLevel.User))
            {
                if (this.viewModel.SelectedMeter != null)
                {
                    this.logic.ExportToPdf(this.viewModel.SelectedMeter, this.viewModel.DateFrom, this.viewModel.DateTo);
                }
            }
            else
            {
                MessageBox.Show("You dont have permission");
            }
        }

        private void SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (this.logic.UserHasThePermissionToExecuteFunction(1, Logic.Enums.EPermissionLevel.User))
            {
                if (sender != null)
                {
                    this.viewModel.SelectedMeter = (sender as TreeView).SelectedItem as Meter;
                    this.viewModel.MeterItemIsSelected = true;
                }
            }
            else
            {
                MessageBox.Show("You dont have permission");
            }
        }
    }
}
