﻿// <copyright file="BasicValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.Validator
{
    using System.Globalization;
    using System.Windows.Controls;

    /// <summary>
    /// A BasicValidator class
    /// </summary>
    public class BasicValidator : ValidationRule
    {
        /// <summary>
        /// Gets or sets the Minimum length of the object
        /// </summary>
        public int MinLength { get; set; }

        /// <summary>
        /// Gets or sets the Maximum length of the object
        /// </summary>
        public int MaxLength { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether that the object can be empty or not.
        /// </summary>
        public bool IsCanBeEmpty { get; set; }

        /// <summary>
        /// Implementation of the Validate logic
        /// </summary>
        /// <param name="value">The value what we want to check</param>
        /// <param name="cultureInfo">The cultureinfo</param>
        /// <returns>Returns A ValidationResult</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (!this.IsCanBeEmpty)
            {
                if (value == null || value.ToString() == string.Empty)
                {
                    return new ValidationResult(false, "Can't be empty!");
                }
            }

            if (this.MinLength > 0)
            {
                if (value.ToString().Length < this.MinLength)
                {
                    return new ValidationResult(false, "Too short!");
                }
            }

            if (value.ToString().Length > this.MaxLength)
            {
                return new ValidationResult(false, "Too long!");
            }

            return new ValidationResult(true, "Perfect!");
        }
    }
}
