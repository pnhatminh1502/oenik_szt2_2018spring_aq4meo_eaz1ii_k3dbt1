﻿// <copyright file="MainWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.ViewModels
{
    using LiveCharts;
    using Logic.Logics;

    /// <summary>
    /// The Main Window's viewmodel
    /// </summary>
    /// <seealso cref="EnergyExplorer.ViewModels.Bindable" />
    public class MainWindowViewModel : Bindable
    {
        private MainWindowLogic logic;
        private SeriesCollection totalConsumptionSeriesCollection;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel"/> class.
        /// </summary>
        /// <param name="logic">MainWindowLogic</param>
        public MainWindowViewModel(MainWindowLogic logic)
        {
            this.logic = logic;
        }

        /// <summary>
        /// Gets or sets of TotalConsumptionSeriesCollection
        /// </summary>
        public SeriesCollection TotalConsumptionSeriesCollection
        {
            get => this.totalConsumptionSeriesCollection;

            set
            {
                this.totalConsumptionSeriesCollection = this.logic.GenerateTariffsReportByEnergyTypeDiagram();
                this.OnPropertyChanged(nameof(this.totalConsumptionSeriesCollection));
            }
        }
    }
}
