﻿// <copyright file="ContractEditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using Data;
    using Data.Model;
    using EnergyExplorer.Enumerators;

    /// <summary>
    /// The contract editor window's viewmodel
    /// </summary>
    /// <seealso cref="EnergyExplorer.ViewModels.Bindable" />
    public class ContractEditorViewModel : Bindable
    {
        private IRepository<Contract> contractRepository;
        private IRepository<EnergyType> energyRepository;
        private IRepository<Company> companyRepository;
        private Contract contract;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractEditorViewModel"/> class.
        /// </summary>
        public ContractEditorViewModel()
        {
            this.Contract = new Contract();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractEditorViewModel"/> class.
        /// </summary>
        /// <param name="contract">The contract.</param>
        public ContractEditorViewModel(Contract contract)
        {
            this.Contract = contract;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractEditorViewModel"/> class.
        /// </summary>
        /// <param name="contractRepository">The contract repository.</param>
        /// <param name="energyRepository">The energy repository.</param>
        /// <param name="companyRepository">The company repository.</param>
        public ContractEditorViewModel(IRepository<Contract> contractRepository, IRepository<EnergyType> energyRepository, IRepository<Company> companyRepository)
        {
            this.contractRepository = contractRepository;
            this.energyRepository = energyRepository;
            this.companyRepository = companyRepository;
        }

        /// <summary>
        /// Gets or sets the contract.
        /// </summary>
        /// <value>
        /// The contract.
        /// </value>
        public Contract Contract
        {
            get
            {
                return this.contract;
            }

            set
            {
                this.contract = value;
                this.OnPropertyChanged(nameof(this.contract));
            }
        }

        /// <summary>
        /// Gets the get all contracts.
        /// </summary>
        /// <value>
        /// The get all contracts.
        /// </value>
        public IList<Contract> GetAllContracts
        {
            get
            {
                return this.contractRepository.GetAll();
            }
        }

        /// <summary>
        /// Gets the get all energy types.
        /// </summary>
        /// <value>
        /// The get all energy types.
        /// </value>
        public IList<EnergyType> GetAllEnergyTypes
        {
            get
            {
                return this.energyRepository.GetAll();
            }
        }

        /// <summary>
        /// Gets the get all companies.
        /// </summary>
        /// <value>
        /// The get all companies.
        /// </value>
        public IList<Company> GetAllCompanies
        {
            get
            {
                return this.companyRepository.GetAll();
            }
        }
    }
}
