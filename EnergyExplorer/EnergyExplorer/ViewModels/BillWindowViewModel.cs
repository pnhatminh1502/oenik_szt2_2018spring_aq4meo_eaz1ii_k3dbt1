﻿// <copyright file="BillWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Data.Model;
    using Logic.Interfaces;
    using Logic.Logics;

    /// <summary>
    /// The BillWindow's viewmodel
    /// </summary>
    /// <seealso cref="EnergyExplorer.ViewModels.Bindable" />
    public class BillWindowViewModel : Bindable
    {
        private IList<Bill> bills;
        private IBillLogic logic;
        private Bill selectedBill;

        /// <summary>
        /// Initializes a new instance of the <see cref="BillWindowViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic.</param>
        public BillWindowViewModel(IBillLogic logic)
        {
            this.logic = logic;
            this.logic.OnListChanged += this.LogicModelListChanged;
        }

        /// <summary>
        /// Gets the bills.
        /// </summary>
        /// <value>
        /// The bills.
        /// </value>
        public IList<Bill> Bills
        {
            get
            {
                return this.logic.GetAll();
            }

            private set
            {
                this.bills = value;
                this.OnPropertyChanged(nameof(this.bills));
            }
        }

        /// <summary>
        /// Gets the selected bill.
        /// </summary>
        /// <value>
        /// The selected bill.
        /// </value>
        public Bill SelectedBill
        {
            get
            {
                return this.selectedBill;
            }

            private set
            {
                this.selectedBill = value;
                this.OnPropertyChanged(nameof(this.selectedBill));
            }
        }

        /// <summary>
        /// This function calls the logic to delete a Bill object.
        /// </summary>
        /// <param name="bill">The bill what we want to delete</param>
        public void Delete(Bill bill)
        {
            this.logic.Remove(bill);
        }

        /// <summary>
        /// Edit a bill object
        /// </summary>
        /// <param name="bill">The bill object what we want to edit</param>
        public void Edit(Bill bill)
        {
            this.logic.Edit(bill);
        }

        /// <summary>
        /// Add a bill object
        /// </summary>
        /// <param name="bill">The bill object what we want to add</param>
        public void Add(Bill bill)
        {
            this.logic.Add(bill);
        }

        /// <summary>
        /// It calls after when the logic makes changes in the database
        /// </summary>
        /// <param name="sender">No</param>
        /// <param name="e">Empty</param>
        private void LogicModelListChanged(object sender, System.EventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Bills));
        }
    }
}