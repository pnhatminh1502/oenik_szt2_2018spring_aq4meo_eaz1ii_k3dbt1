﻿// <copyright file="ContractWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Data.Model;
    using Logic;
    using Logic.Interfaces;

    /// <summary>
    /// The contract window's viewmodel
    /// </summary>
    /// <seealso cref="EnergyExplorer.ViewModels.Bindable" />
    public class ContractWindowViewModel : Bindable
    {
        private IList<Contract> contracts;
        private IContractLogic logic;
        private Contract selectedContract;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractWindowViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic.</param>
        public ContractWindowViewModel(IContractLogic logic)
        {
            this.logic = logic;
            this.logic.OnListChanged += this.LogicModelListChanged;
        }

        /// <summary>
        /// Gets or sets the contracts.
        /// </summary>
        /// <value>
        /// The contracts.
        /// </value>
        public IList<Contract> Contracts
        {
            get
            {
                return this.logic.GetAll();
            }

            set
            {
                this.contracts = value;
                this.OnPropertyChanged(nameof(this.contracts));
            }
        }

        /// <summary>
        /// Gets the selected contract.
        /// </summary>
        /// <value>
        /// The selected contract.
        /// </value>
        public Contract SelectedContract
        {
            get
            {
                return this.selectedContract;
            }

            private set
            {
                this.selectedContract = value;
                this.OnPropertyChanged(nameof(this.selectedContract));
            }
        }

        /// <summary>
        /// Edits the specified contract.
        /// </summary>
        /// <param name="contract">The contract.</param>
        public void Edit(Contract contract)
        {
            this.logic.Edit(contract);
        }

        /// <summary>
        /// Adds the specified contract.
        /// </summary>
        /// <param name="contract">The contract.</param>
        public void Add(Contract contract)
        {
            this.logic.Add(contract);
        }

        /// <summary>
        /// Deletes the specified contract.
        /// </summary>
        /// <param name="contract">The contract.</param>
        public void Delete(Contract contract)
        {
            this.logic.Remove(contract);
        }

        /// <summary>
        /// Logics the model list changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void LogicModelListChanged(object sender, System.EventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Contracts));
        }
    }
}
