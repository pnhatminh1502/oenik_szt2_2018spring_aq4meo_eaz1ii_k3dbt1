﻿// <copyright file="LoginWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace EnergyExplorer.ViewModels
{
    using System.Collections.Generic;
    using Logic.Logics;

    /// <summary>
    /// The LoginWindow's ViewModel
    /// </summary>
    /// <seealso cref="EnergyExplorer.ViewModels.Bindable" />
    public class LoginWindowViewModel : Bindable
    {
        private LoginWindowLogic userLogic;
        private string username;
        private string password;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginWindowViewModel"/> class.
        /// </summary>
        /// <param name="userLogic">The user logic.</param>
        public LoginWindowViewModel(LoginWindowLogic userLogic)
        {
            this.userLogic = userLogic;
        }

        /// <summary>
        /// Gets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username
        {
            get
            {
                return this.username;
            }

            private set
            {
                this.username = value;
                this.OnPropertyChanged(nameof(this.username));
            }
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                this.password = value;

                // this.OnPropertyChanged(nameof(this.password));
            }
        }
    }
}
