﻿// <copyright file="BillEditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.ViewModels
{
    using System.Collections.Generic;
    using Data;
    using Data.Model;

    /// <summary>
    /// The bill editor's viewmodel
    /// </summary>
    /// <seealso cref="EnergyExplorer.ViewModels.Bindable" />
    public class BillEditorViewModel : Bindable
    {
        private IRepository<Contract> contractRepository;
        private IRepository<Meter> meterRepository;
        private Bill bill;

        /// <summary>
        /// Initializes a new instance of the <see cref="BillEditorViewModel"/> class.
        /// </summary>
        public BillEditorViewModel()
        {
            this.Bill = new Bill();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BillEditorViewModel"/> class.
        /// </summary>
        /// <param name="bill">The bill.</param>
        public BillEditorViewModel(Bill bill)
        {
            this.Bill = bill;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BillEditorViewModel"/> class.
        /// </summary>
        /// <param name="meterRepository">The meter repository.</param>
        /// <param name="contractRepository">The contract repository.</param>
        public BillEditorViewModel(IRepository<Meter> meterRepository, IRepository<Contract> contractRepository)
        {
            this.meterRepository = meterRepository;
            this.contractRepository = contractRepository;
        }

        /// <summary>
        /// Gets or sets the bill.
        /// </summary>
        /// <value>
        /// The bill.
        /// </value>
        public Bill Bill
        {
            get
            {
                return this.bill;
            }

            set
            {
                this.bill = value;
                this.OnPropertyChanged(nameof(this.bill));
            }
        }

        /// <summary>
        /// Gets the get all contracts.
        /// </summary>
        /// <value>
        /// The get all contracts.
        /// </value>
        public IList<Contract> GetAllContracts
        {
            get
            {
                return this.contractRepository.GetAll();
            }
        }

        /// <summary>
        /// Gets the get all meters.
        /// </summary>
        /// <value>
        /// The get all meters.
        /// </value>
        public IList<Meter> GetAllMeters
        {
            get
            {
                return this.meterRepository.GetAll();
            }
        }
    }
}