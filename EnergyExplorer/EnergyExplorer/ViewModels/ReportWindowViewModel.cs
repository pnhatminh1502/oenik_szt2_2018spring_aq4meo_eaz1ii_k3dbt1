﻿// <copyright file="ReportWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EnergyExplorer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using Data.Model;
    using LiveCharts;
    using Logic.Interfaces;

    /// <summary>
    /// The ReportWindow's viewmodel
    /// </summary>
    /// <seealso cref="EnergyExplorer.ViewModels.Bindable" />
    public class ReportWindowViewModel : Bindable
    {
        private IReportLogic logic;
        private DateTime dateFrom;
        private DateTime dateTo;
        private Meter selectedMeter;
        private SeriesCollection meter15mSeriesCollection;
        private bool meterItemIsSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportWindowViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic.</param>
        public ReportWindowViewModel(IReportLogic logic)
        {
            this.logic = logic;
            this.SelectedMeter = null;
        }

        /// <summary>
        /// Gets the meters.
        /// </summary>
        /// <value>
        /// The meters.
        /// </value>
        public IList<Meter> Meters
        {
            get
            {
                return this.logic.GetAllMeters();
            }
        }

        /// <summary>
        /// Gets or sets the value of SelectedMeter
        /// </summary>
        public Meter SelectedMeter
        {
            get
            {
                return this.selectedMeter;
            }

            set
            {
                this.selectedMeter = value;
                this.OnPropertyChanged(nameof(this.selectedMeter));
            }
        }

        /// <summary>
        /// Gets or sets gets the date from.
        /// </summary>
        /// <value>
        /// The date from.
        /// </value>
        public DateTime DateFrom
        {
            get => this.dateFrom;

            set
            {
                this.dateFrom = value;
                this.OnPropertyChanged(nameof(this.dateFrom));
            }
        }

        /// <summary>
        /// Gets or sets gets the date to.
        /// </summary>
        /// <value>
        /// The date to.
        /// </value>
        public DateTime DateTo
        {
            get => this.dateTo;

            set
            {
                this.dateTo = value;
                this.OnPropertyChanged(nameof(this.dateTo));
            }
        }

        public SeriesCollection Meter15mSeriesCollection
        {
            get
            {
                return this.meter15mSeriesCollection;
            }

            set
            {
                this.meter15mSeriesCollection = this.logic.GenerateMeter15mSeriesForReportDiagram(this.DateFrom, this.dateTo, this.SelectedMeter.id);
                this.OnPropertyChanged(nameof(this.meter15mSeriesCollection));
            }
        }

        /// <summary>
        /// It calls a logic function to generate a Meter15m Series based on the chosen period of time and the meter type
        /// </summary>
        public void GenerateMeter15mSeriesCollection()
        {
            if (this.dateFrom != null && this.dateTo != null && this.SelectedMeter != null)
            {
                this.Meter15mSeriesCollection = this.logic.GenerateMeter15mSeriesForReportDiagram(this.DateFrom, this.dateTo, this.SelectedMeter.id);
            }
        }

        /// <summary>
        /// It calls a logic function to show location of the meter on the google maps
        /// </summary>
        public void ShowLocationOnGoogleMaps()
        {
            this.logic.ShowMeterLocationInGoogleMaps(this.selectedMeter);
        }

        /// <summary>
        /// Gets or sets a value indicating whether [meter item is selected].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [meter item is selected]; otherwise, <c>false</c>.
        /// </value>
        public bool MeterItemIsSelected
        {
            get
            {
                return this.meterItemIsSelected;
            }

            set
            {
                this.meterItemIsSelected = value;
                this.OnPropertyChanged(nameof(this.meterItemIsSelected));
            }
        }
    }
}
