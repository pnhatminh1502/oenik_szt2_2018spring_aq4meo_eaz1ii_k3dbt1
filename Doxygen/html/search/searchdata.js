var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvwx",
  1: "abcdegilmrstu",
  2: "deltx",
  3: "abcdefgilmorstuv",
  4: "e",
  5: "aeghuw",
  6: "bcdgimpstu",
  7: "op",
  8: "cn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties",
  7: "events",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties",
  7: "Events",
  8: "Pages"
};

