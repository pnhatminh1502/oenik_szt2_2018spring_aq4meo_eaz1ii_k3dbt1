var searchData=
[
  ['user',['User',['../class_data_1_1_model_1_1_user.html',1,'Data.Model.User'],['../namespace_logic_1_1_enums.html#a34adcb18c2bb314c4fb6917759fcdb78a8f9bfe9d1345237cb3b2b205864da075',1,'Logic.Enums.User()']]],
  ['userhasthepermissiontoexecutefunction',['UserHasThePermissionToExecuteFunction',['../interface_logic_1_1_interfaces_1_1_i_permission.html#acfed6f0db9bfaf722e32367d21e73d1c',1,'Logic.Interfaces.IPermission.UserHasThePermissionToExecuteFunction()'],['../class_logic_1_1_logics_1_1_bill_window_logic.html#a56f5a8ed3b06156a9864fa148a259d0f',1,'Logic.Logics.BillWindowLogic.UserHasThePermissionToExecuteFunction()'],['../class_logic_1_1_contract_window_logic.html#a4a6d462f02944c3a0d12b29ce6508a69',1,'Logic.ContractWindowLogic.UserHasThePermissionToExecuteFunction()'],['../class_logic_1_1_report_window_logic.html#aeb9e755c709d1a0625c7e65ecce4fa11',1,'Logic.ReportWindowLogic.UserHasThePermissionToExecuteFunction()']]],
  ['username',['Username',['../class_energy_explorer_1_1_view_models_1_1_login_window_view_model.html#a35fdf42397246ae1e7e87bf7f25c7bab',1,'EnergyExplorer::ViewModels::LoginWindowViewModel']]]
];
