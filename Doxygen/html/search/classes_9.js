var searchData=
[
  ['reportwindow',['ReportWindow',['../class_energy_explorer_1_1_windows_1_1_report_window.html',1,'EnergyExplorer::Windows']]],
  ['reportwindowlogic',['ReportWindowLogic',['../class_logic_1_1_report_window_logic.html',1,'Logic']]],
  ['reportwindowviewmodel',['ReportWindowViewModel',['../class_energy_explorer_1_1_view_models_1_1_report_window_view_model.html',1,'EnergyExplorer::ViewModels']]],
  ['reportwindowviewmodeltests',['ReportWindowViewModelTests',['../class_test_1_1_view_model_tests_1_1_report_window_view_model_tests.html',1,'Test::ViewModelTests']]],
  ['repository',['Repository',['../class_data_1_1_repository_1_1_repository.html',1,'Data::Repository']]],
  ['repository_3c_20data_3a_3amodel_3a_3aenergytype_20_3e',['Repository&lt; Data::Model::EnergyType &gt;',['../class_data_1_1_repository_1_1_repository.html',1,'Data::Repository']]],
  ['repository_3c_20data_3a_3amodel_3a_3atariff_20_3e',['Repository&lt; Data::Model::Tariff &gt;',['../class_data_1_1_repository_1_1_repository.html',1,'Data::Repository']]],
  ['repository_3c_20data_3a_3amodel_3a_3auser_20_3e',['Repository&lt; Data::Model::User &gt;',['../class_data_1_1_repository_1_1_repository.html',1,'Data::Repository']]]
];
